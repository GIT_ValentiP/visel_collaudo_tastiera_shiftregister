/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "input_keyboard.h"
#include "led_driver_PCA9685.h"
#include "globals.h"

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */



/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */
extern GPIO_TypeDef* switch_port[];




/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define TIMER6_RELOAD_TO_10us 65525
#define TIMER6_PRESCALER_1MHz 16
#define COUNT_TO_1ms 100
#define COUNT_TO_100ms 10000
#define OPERA_RS485_ADDR 16
#define RS485_BAUDRATE 39200
#define RS485_CMD_RD_TASTI 32
#define RS485_RD_ENCODER 48
#define RS485_CMD_WR_DOSE1_COL 33
#define RS485_CMD_WR_DOSE2_COL 34
#define RS485_CMD_WR_DOSE3_COL 35
#define RS485_CMD_WR_MAN_COL 36
#define RES485_CMD_WR_DISPLAY 49
#define TIMER7_RELOAD_TO_10ms 55535
#define OSC_EXT_FREQ 16000000
#define SW2_RIGHT_T6_Pin GPIO_PIN_13
#define SW2_RIGHT_T6_GPIO_Port GPIOC
#define SW2_CENTER_T5_Pin GPIO_PIN_14
#define SW2_CENTER_T5_GPIO_Port GPIOC
#define SW2_LEFT_T4_Pin GPIO_PIN_15
#define SW2_LEFT_T4_GPIO_Port GPIOC
#define CURRENT_KEYS_ADC10_Pin GPIO_PIN_0
#define CURRENT_KEYS_ADC10_GPIO_Port GPIOC
#define SW1_CENTER_T2_Pin GPIO_PIN_1
#define SW1_CENTER_T2_GPIO_Port GPIOC
#define SW1_RIGHT_T3_Pin GPIO_PIN_1
#define SW1_RIGHT_T3_GPIO_Port GPIOC
#define SW1_LEFT_T1_Pin GPIO_PIN_2
#define SW1_LEFT_T1_GPIO_Port GPIOC
#define SPI2_MOSI_SDI_LED_Pin GPIO_PIN_3
#define SPI2_MOSI_SDI_LED_GPIO_Port GPIOC
#define RS485_RE_UART2CTS_Pin GPIO_PIN_0
#define RS485_RE_UART2CTS_GPIO_Port GPIOA
#define RS485_DE_UART2RTS_Pin GPIO_PIN_1
#define RS485_DE_UART2RTS_GPIO_Port GPIOA
#define RS485_MCU_TX_Pin GPIO_PIN_2
#define RS485_MCU_TX_GPIO_Port GPIOA
#define RS485_MCU_RX_Pin GPIO_PIN_3
#define RS485_MCU_RX_GPIO_Port GPIOA
#define ENCODER_CHA_Pin GPIO_PIN_4
#define ENCODER_CHA_GPIO_Port GPIOA
#define ENCODER_CHA_EXTI_IRQn EXTI4_15_IRQn
#define ENCODER_CHB_Pin GPIO_PIN_5
#define ENCODER_CHB_GPIO_Port GPIOA
#define ENCODER_CHB_EXTI_IRQn EXTI4_15_IRQn
#define SEG_A1_1_Pin GPIO_PIN_6
#define SEG_A1_1_GPIO_Port GPIOA
#define SEG_F_4_Pin GPIO_PIN_7
#define SEG_F_4_GPIO_Port GPIOA
#define SEG_DP_12_Pin GPIO_PIN_4
#define SEG_DP_12_GPIO_Port GPIOC
#define SEG_N_7_Pin GPIO_PIN_5
#define SEG_N_7_GPIO_Port GPIOC
#define SEG_M_6_Pin GPIO_PIN_0
#define SEG_M_6_GPIO_Port GPIOB
#define SEG_L_14_Pin GPIO_PIN_1
#define SEG_L_14_GPIO_Port GPIOB
#define SEG_K_17_Pin GPIO_PIN_2
#define SEG_K_17_GPIO_Port GPIOB
#define SPI2_SCK_SHIFT_CLK_Pin GPIO_PIN_10
#define SPI2_SCK_SHIFT_CLK_GPIO_Port GPIOB
#define SEG_H_3_Pin GPIO_PIN_11
#define SEG_H_3_GPIO_Port GPIOB
#define SPI2_NSS_STROBECLK_Pin GPIO_PIN_12
#define SPI2_NSS_STROBECLK_GPIO_Port GPIOB
#define SEG_G1_5_Pin GPIO_PIN_13
#define SEG_G1_5_GPIO_Port GPIOB
#define SPI2_MISO_SDO_KEYS_Pin GPIO_PIN_14
#define SPI2_MISO_SDO_KEYS_GPIO_Port GPIOB
#define SEG_E_8_Pin GPIO_PIN_15
#define SEG_E_8_GPIO_Port GPIOB
#define SEG_D2_10_Pin GPIO_PIN_6
#define SEG_D2_10_GPIO_Port GPIOC
#define SEG_D1_9_Pin GPIO_PIN_7
#define SEG_D1_9_GPIO_Port GPIOC
#define SEG_C_13_Pin GPIO_PIN_8
#define SEG_C_13_GPIO_Port GPIOC
#define SEG_B_16_Pin GPIO_PIN_9
#define SEG_B_16_GPIO_Port GPIOC
#define SEG_A2_18_Pin GPIO_PIN_8
#define SEG_A2_18_GPIO_Port GPIOA
#define RS232_TO_PC_UART1_TX_Pin GPIO_PIN_9
#define RS232_TO_PC_UART1_TX_GPIO_Port GPIOA
#define RS232_FROM_PC_UART1_RX_Pin GPIO_PIN_10
#define RS232_FROM_PC_UART1_RX_GPIO_Port GPIOA
#define JTAG_SWDIO_Pin GPIO_PIN_13
#define JTAG_SWDIO_GPIO_Port GPIOA
#define JTAG_SWCLK_Pin GPIO_PIN_14
#define JTAG_SWCLK_GPIO_Port GPIOA
#define SEG_G2_15_Pin GPIO_PIN_15
#define SEG_G2_15_GPIO_Port GPIOA
#define SW3_LEFT_T7_Pin GPIO_PIN_10
#define SW3_LEFT_T7_GPIO_Port GPIOC
#define SW3_CENTER_T8_Pin GPIO_PIN_11
#define SW3_CENTER_T8_GPIO_Port GPIOC
#define SW3_RIGHT_T9_Pin GPIO_PIN_12
#define SW3_RIGHT_T9_GPIO_Port GPIOC
#define SW4_CENTER_T11_Pin GPIO_PIN_2
#define SW4_CENTER_T11_GPIO_Port GPIOD
#define SW4_LEFT_T10_Pin GPIO_PIN_2
#define SW4_LEFT_T10_GPIO_Port GPIOD
#define SDO_LED_SDI_KEYS_Pin GPIO_PIN_3
#define SDO_LED_SDI_KEYS_GPIO_Port GPIOB
#define SW4_RIGHT_T12_Pin GPIO_PIN_4
#define SW4_RIGHT_T12_GPIO_Port GPIOB
#define LED_DRIVER__OE_Pin GPIO_PIN_5
#define LED_DRIVER__OE_GPIO_Port GPIOB
#define LED_DRIVER_SCL_Pin GPIO_PIN_6
#define LED_DRIVER_SCL_GPIO_Port GPIOB
#define LED_DRIVER_SDA_Pin GPIO_PIN_7
#define LED_DRIVER_SDA_GPIO_Port GPIOB
#define RST_STORE_LED_Pin GPIO_PIN_8
#define RST_STORE_LED_GPIO_Port GPIOB
#define SEG_J_2_Pin GPIO_PIN_9
#define SEG_J_2_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

#define SEG_A1_Pin                           GPIO_PIN_6
#define SEG_A1_GPIO_Port                          GPIOA
#define SEG_A2_Pin                           GPIO_PIN_8
#define SEG_A2_GPIO_Port                          GPIOA
#define SEG_B_Pin                            GPIO_PIN_9
#define SEG_B_GPIO_Port                          GPIOC
#define SEG_C_Pin                           GPIO_PIN_8
#define SEG_C_GPIO_Port                          GPIOC
#define SEG_D1_Pin                           GPIO_PIN_7
#define SEG_D1_GPIO_Port                          GPIOC
#define SEG_D2_Pin                          GPIO_PIN_6
#define SEG_D2_GPIO_Port                         GPIOC
#define SEG_E_Pin                           GPIO_PIN_15
#define SEG_E_GPIO_Port                           GPIOB
#define SEG_F_Pin                            GPIO_PIN_7
#define SEG_F_GPIO_Port                           GPIOA
#define SEG_G1_Pin                          GPIO_PIN_13
#define SEG_G1_GPIO_Port                          GPIOB
#define SEG_G2_Pin                         GPIO_PIN_15
#define SEG_G2_GPIO_Port                         GPIOA
#define SEG_H_Pin                           GPIO_PIN_11
#define SEG_H_GPIO_Port                           GPIOB
#define SEG_J_Pin                            GPIO_PIN_9
#define SEG_J_GPIO_Port                           GPIOB
#define SEG_K_Pin                           GPIO_PIN_2
#define SEG_K_GPIO_Port                          GPIOB
#define SEG_L_Pin                           GPIO_PIN_1
#define SEG_L_GPIO_Port                          GPIOB
#define SEG_M_Pin                            GPIO_PIN_0
#define SEG_M_GPIO_Port                           GPIOB
#define SEG_N_Pin                            GPIO_PIN_5
#define SEG_N_GPIO_Port                           GPIOC
#define SEG_DP_Pin                          GPIO_PIN_4
#define SEG_DP_GPIO_Port                         GPIOC

/*TIMER 6*/

#define RS485_TX_ENABLE     HAL_GPIO_WritePin(RS485_DE_UART2RTS_GPIO_Port,RS485_DE_UART2RTS_Pin,GPIO_PIN_SET)
#define RS485_TX_DISABLE    HAL_GPIO_WritePin(RS485_DE_UART2RTS_GPIO_Port,RS485_DE_UART2RTS_Pin,GPIO_PIN_RESET)
#define RS485_RX_ENABLE     HAL_GPIO_WritePin(RS485_RE_UART2CTS_GPIO_Port,RS485_RE_UART2CTS_Pin,GPIO_PIN_RESET)
#define RS485_RX_DISABLE    HAL_GPIO_WritePin(RS485_RE_UART2CTS_GPIO_Port,RS485_RE_UART2CTS_Pin,GPIO_PIN_SET)



/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
