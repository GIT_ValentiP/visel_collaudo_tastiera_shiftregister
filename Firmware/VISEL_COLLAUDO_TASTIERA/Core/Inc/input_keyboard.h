/*
 ******************************************************************************
 *  @file      : input_keyboard.h
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 22 lug 2019
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : SANREMO_Opera 
 *   
 *  @brief     : Cortex-M0 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */

#ifndef INC_INPUT_KEYBOARD_H_
#define INC_INPUT_KEYBOARD_H_


/* Includes ----------------------------------------------------------*/
#include "main.h"
/* Typedef -----------------------------------------------------------*/
//typedef struct {
//		uint16_t sw1_l:1;
//		uint16_t sw1_c:1;
//		uint16_t sw1_r:1;
//		uint16_t sw2_l:1;
//		uint16_t sw2_c:1;
//		uint16_t sw2_r:1;
//		uint16_t sw3_l:1;
//		uint16_t sw3_c:1;
//		uint16_t sw3_r:1;
//		uint16_t sw4_l:1;
//		uint16_t sw4_c:1;
//		uint16_t sw4_r:1;
//		uint16_t sw5_l:1;
//		uint16_t sw5_c:1;
//		uint16_t sw5_r:1;
//		uint16_t sw_free1:1;
//} integer_bit_struct_t;
/**
 * @brief
 */

/**
 * @brief
 */
typedef enum {
		SW1_L, /*!< SW1_LEFT function */
		SW1_C, /*!< SW1_LEFT function */
		SW1_R, /*!< SW1_LEFT function */
		SW2_L, /*!< SW1_LEFT function */
		SW2_C, /*!< SW1_LEFT function */
		SW2_R, /*!< SW1_LEFT function */
		SW3_L, /*!< SW1_LEFT function */
		SW3_C, /*!< SW1_LEFT function */
		SW3_R, /*!< SW1_LEFT function */
		SW4_L, /*!< SW1_LEFT function */
		SW4_C, /*!< SW1_LEFT function */
		SW4_R, /*!< SW1_LEFT function */
} SWITCHES_Func_t;
/* Define ------------------------------------------------------------*/
#define NUM_MAX_SWITCHES       12
#define DEBOUNCE_SW_REF1_ms    2500       /*in milliseconds*/
#define DEBOUNCE_SW_REF2_ms    3500       /*in milliseconds*/
#define DEBOUNCE_SW_REF3_ms    5000       /*in milliseconds*/
#define DEBOUNCE_SW_RELEASE_ms    5       /*in milliseconds*/
/* Macro -------------------------------------------------------------*/


/* Variables ---------------------------------------------------------*/
uint16_t sw_debounce1_counter[NUM_MAX_SWITCHES];
uint16_t sw_debounce2_counter[NUM_MAX_SWITCHES];
uint16_t sw_debounce3_counter[NUM_MAX_SWITCHES];
uint16_t   sw_deb_release_cnt[NUM_MAX_SWITCHES];
uint16_t switches_bitmap_deb1;
uint16_t switches_bitmap_deb2;
uint16_t switches_bitmap_deb3;
uint16_t switches_bitmap_actual;

//GPIO_TypeDef* switch_port[NUM_MAX_SWITCHES]={SW1_LEFT_T1_GPIO_Port,SW1_CENTER_T2_GPIO_Port ,SW1_RIGHT_T3_GPIO_Port,SW2_LEFT_T4_GPIO_Port,SW2_CENTER_T5_GPIO_Port ,SW2_RIGHT_T6_GPIO_Port,SW3_LEFT_T7_GPIO_Port,SW3_CENTER_T8_GPIO_Port ,SW3_RIGHT_T9_GPIO_Port,SW4_LEFT_T10_GPIO_Port,SW4_CENTER_T11_GPIO_Port ,SW4_RIGHT_T12_GPIO_Port};
//const uint16_t      switch_pin[NUM_MAX_SWITCHES] ={SW1_LEFT_T1_Pin,SW1_CENTER_T2_Pin ,SW1_RIGHT_T3_Pin,SW2_LEFT_T4_Pin,SW2_CENTER_T5_Pin ,SW2_RIGHT_T6_Pin,SW3_LEFT_T7_Pin,SW3_CENTER_T8_Pin ,SW3_RIGHT_T9_Pin,SW4_LEFT_T10_Pin,SW4_CENTER_T11_Pin ,SW4_RIGHT_T12_Pin};

/* Function prototypes -----------------------------------------------*/
/**
 * @brief  Initialize function for input keyboard switches
 * @note   This function inizialize all variables to manage inputs switches
 * @param  None
 * @retval None
 */
void init_input_reayboard(void);

/**
 * @brief  Read and debounce function.
 * @note   This function have to be called in.
 * @param  None
 * @retval None
 */
void read_input_keyboard(void);

void Opera_keyboard_function_7Tasti(void);
void Opera_keyboard_function_4Tasti(void);
/**
 * @brief  Read and debounce function.
 * @note   This function have to be called in.
 * @param  None
 * @retval None
 */
unsigned char keyboard_function_manager(void);

void Viesel_function_4Tasti(void);
void scan_read_Visel(void);
void scan_if_pressed_Visel(void);
void clear_test_Visel(void);

#endif /* INC_INPUT_KEYBOARD_H_ */
