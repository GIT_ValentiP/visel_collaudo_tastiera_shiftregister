/*
 * globals.h
 *
 *  Created on: 07 ago 2019
 *      Author: VALENTI
 */

#ifndef INC_GLOBALS_H_
#define INC_GLOBALS_H_


#include "main.h"


#define MSG_NUM_CHAR_MAX 10
#define NUM_OUT_SHIFTREG  40
#define NUM_MAX_SHIFTREG   5

#define CURRENT_KEY_MAX_mV 25
// types
typedef union{
	struct byte_bit_s{
			uint8_t b0:1;
			uint8_t b1:1;
			uint8_t b2:1;
			uint8_t b3:1;
			uint8_t b4:1;
			uint8_t b5:1;
			uint8_t b6:1;
			uint8_t b7:1;
	}bit;
	    uint8_t bytes;
}byte_bit_t;

typedef struct system_timer_s
{
	double tick_10ms;              //
    double tick_100ms;            //
    double seconds;               //
    double mins;                  //
    double hours;                 //
} sys_timer_t;

typedef struct {
			uint8_t flag_10ms:1;
			uint8_t flag_100ms:1;
			uint8_t flag_3:1;
			uint8_t flag_1sec:1;
			uint8_t flag_5sec:1;
			uint8_t flag_6:1;
			uint8_t flag_7:1;
			uint8_t flag_1:1;

}timer_flag_t;
typedef union{
	struct int_bit_s{
			uint16_t sw1_l:1;
			uint16_t sw1_c:1;
			uint16_t sw1_r:1;
			uint16_t sw2_l:1;
			uint16_t sw2_c:1;
			uint16_t sw2_r:1;
			uint16_t sw3_l:1;
			uint16_t sw3_c:1;
			uint16_t sw3_r:1;
			uint16_t sw4_l:1;
			uint16_t sw4_c:1;
			uint16_t sw4_r:1;
			uint16_t sw5_l:1;
			uint16_t sw5_c:1;
			uint16_t sw5_r:1;
			uint16_t sw_free1:1;
	}sw;

	    uint16_t bitmap;
}Keys_Func_t;


typedef union{
	struct {
			uint8_t sw1:1;
			uint8_t sw2:1;
			uint8_t sw3:1;
			uint8_t sw4:1;
			uint8_t sw5:1;
			uint8_t sw6:1;
			uint8_t sw7:1;
			uint8_t sw8:1;
	}sw;

	uint8_t raw;
}Keys_Visel_t;

typedef enum {
			VISEL_IDLE=0x00,
			VISEL_START_TEST=0x01,
			VISEL_RUN=0x02,
			VISEL_END_TEST=0x03,
			VISEL_RESTART_TEST=0x04,
			VISEL_TEST1=0x10,
			VISEL_TEST2=0x20,
			VISEL_TEST3=0x30,
			VISEL_TEST4=0x40,
			VISEL_ERROR=0x80,
}VISEL_STATUS_enum_t;

typedef struct {
	uint8_t tot_press;
	bool btn[NUM_OUT_SHIFTREG];
	VISEL_STATUS_enum_t status;
	byte_bit_t err;
}KeysTest_Visel_t;


typedef union{
	struct func_bit_s{
			uint16_t dose1:2;
			uint16_t dose2:2;
			uint16_t dose3:2;
			uint16_t dose4:2;
			uint16_t dose5:2;
			uint16_t dose6:2;
			uint16_t dose7:2;
			uint16_t dose8:2;
	}sw;

	    uint16_t bitmap;
}Keys_Func_Opera_t;






typedef struct {
			uint16_t duty_RED;
			uint16_t duty_GREEN;
			uint16_t duty_BLUE;
			uint8_t id_col;
}RGB_LED_t;

typedef enum {
			NOTHING_TO_READ,
			ENCODER_READ,
			KEYBOARD_BITMAP_READ,
			KEYBOARD_LOGIC_READ,
			FW_REL_READ,
			OPERA_STATUS_READ
}RS485_PARAM_READ_enum_t;

typedef enum {
			OPERA_NOT_CONFIG=0x0000,
			OPERA_READY=0x0001,
			OPERA_RESET_TO_DEFAULT=0x0002,
			OPERA_ERROR=0x0003
}OPERA_STATUS_enum_t;

typedef enum {
			OPERA_4_TASTI=0x0000,
			OPERA_7_TASTI=0x0001,
			OPERA_NOT_SET=0xFFFF
}OPERA_CONFIG_enum_t;



typedef struct {
			uint8_t msg[MSG_NUM_CHAR_MAX];
			uint16_t time_refresh;
			uint8_t char_active;
			uint8_t len_msg;
			uint8_t status;
}MSG_DISPLAY_t;

typedef enum {
	        MSG_DISP_OFF=0,
			MSG_DISP_ACTIVE=1,
			MSG_DISP_ERR=100,
}DISP_STATUS_enum_t;


typedef struct {
			uint32_t raw;
			uint32_t threshold;//mVolts
			uint8_t  nsample;
			uint32_t avg;
			uint32_t mVolt;
			/* Variable to report status of ADC group regular unitary conversion          */
			/*  0: ADC group regular unitary conversion is not completed                  */
			/*  1: ADC group regular unitary conversion is completed                      */
			/*  2: ADC group regular unitary conversion has not been started yet          */
			/*     (initial state)                                                        */
			uint8_t AdcConvStatus ; /* Variable set into ADC interruption callback */
}ADC_CHANNEL_t;


#define TIME_REFRESH_DISP_SEC 1 //sec time fix char in display
#define TIME_REFRESH_WORD_SEC 5

#ifndef TRUE
#define TRUE            1
#endif

#ifndef FALSE
#define FALSE           0
#endif

#define BIT_0         (0x01U)
#define BIT_1         (0x02U)
#define BIT_2         (0x04U)
#define BIT_3         (0x08U)
#define BIT_4         (0x10U)
#define BIT_5         (0x20U)
#define BIT_6         (0x40U)
#define BIT_7         (0x80U)

#define BIT_8         (0x0100U)
#define BIT_9         (0x0200U)
#define BIT_10        (0x0400U)
#define BIT_11        (0x0800U)
#define BIT_12        (0x1000U)
#define BIT_13        (0x2000U)
#define BIT_14        (0x4000U)
#define BIT_15        (0x8000U)

#define BIT_16        (0x010000U)
#define BIT_17        (0x020000U)
#define BIT_18        (0x040000U)
#define BIT_19        (0x080000U)
#define BIT_20        (0x100000U)
#define BIT_21        (0x200000U)
#define BIT_22        (0x400000U)
#define BIT_23        (0x800000U)

#define BIT_24        (0x01000000U)
#define BIT_25        (0x02000000U)
#define BIT_26        (0x04000000U)
#define BIT_27        (0x08000000U)
#define BIT_28        (0x10000000U)
#define BIT_29        (0x20000000U)
#define BIT_30        (0x40000000U)
#define BIT_31        (0x80000000U)


/*MODBUS HOLDING REGISTER ADDRESS*/
#define MDB_ENCODER_REG                                       0x203
#define MDB_FW_REL_REG                                        0x100
#define MDB_KEYBOARD_BITMAP_REG                               0x200
#define MDB_KEYBOARD_FUNCTION_REG                             0x202
#define MDB_OPERA_STATUS_REG                                  0x201
#define MDB_LED_DOSE1_RED_REG                                 0x300
#define MDB_LED_DOSE1_GREEN_REG                               0x301
#define MDB_LED_DOSE1_BLUE_REG                                0x302
#define MDB_LED_DOSE2_RED_REG                                 0x303
#define MDB_LED_DOSE2_GREEN_REG                               0x304
#define MDB_LED_DOSE2_BLUE_REG                                0x305
#define MDB_LED_DOSE3_RED_REG                                 0x306
#define MDB_LED_DOSE3_GREEN_REG                               0x307
#define MDB_LED_DOSE3_BLUE_REG                                0x308
#define MDB_LED_DOSE_MAN_RED_REG                              0x309
#define MDB_LED_DOSE_MAN_GREEN_REG                            0x30A
#define MDB_LED_DOSE_MAN_BLUE_REG                             0x30B
#define MDB_DISPLAY16SEG_REG                                  0x30C
#define MDB_LED5_RED_REG                                      0x400
#define MDB_LED5_GREEN_REG                                    0x401
#define MDB_LED5_BLUE_REG                                     0x402
#define MDB_LED6_WHITE_REG                                    0x403

#define MDB_KEYBOARD_CONFIG_REG                              0x1000
#define MDB_TIME_LONG_PRESS_REG                              0x1001
#define MDB_TIME_INTERCLICK_PRESS_REG                        0x1002



#define TIMEOUT_100ms_NO_MSG_MDB                  5//in 100ms




#define NUM_LED_RGB 6
#define SW1_LED_RGB 0
#define SW2_LED_RGB 1
#define SW3_LED_RGB 2
#define SW4_LED_RGB 3
#define SW5_LED_RGB 4
#define SW6_LED_RGB 5

#define LEN_RX_RS232 6
#define LEN_TX_RS232 6
#define RGB_DELTA_TRANSITION 615//15% duty //205 5% duty



#define NO_KEY_PRESSED          0u
#define LEFT_SINGLE_KEY_PRESSED 1u
#define RIGHT_DOUBLE_PRESSED    2u
#define KEY_LONG_PRESSED        3u
/*MODBUS Registers*/


/* ----------------------- Static variables ---------------------------------*/

uint8_t buffer_RX_RS232[10];
uint8_t buffer_TX_RS232[10];

unsigned char char_rx_to_display,led_to_change,duty_red,duty_green,duty_blue;
RS485_PARAM_READ_enum_t parameter_to_read;
bool new_data_rx;
bool wait_reply;
bool change_state;
unsigned int timeout_mdb_no_msg;

unsigned char TOGGLE_PIN_LED;

unsigned char read_keyboard_event;
unsigned char display_refresh_event;
unsigned char display_refresh_start_event;
unsigned char PCA9685_delay_event;
/*Clock System timer Variables*/
sys_timer_t main_clock;

unsigned char char_disp_17seg;
RGB_LED_t led_RGB[NUM_LED_RGB];
unsigned int  RGB_color_start[3];
unsigned char RGB_Transition;

unsigned int short_press_time;
unsigned int long_press_time;
unsigned int click_double_press_time;

Keys_Func_t keyboard_raw;
Keys_Func_t keyboard_short;
Keys_Func_t keyboard_long;

Keys_Func_t keyboard_1click;
Keys_Func_t keyboard_2click;

Keys_Func_Opera_t keyboard_dose;

uint32_t version_fw;

uint16_t interclik_time_ms;
uint16_t longpress_time_ms;
uint16_t adr_modbus_reg;
OPERA_STATUS_enum_t opera_state;
OPERA_CONFIG_enum_t opera_keyboard_conf;

uint8_t  DP_SW_RS485_adr;

uint32_t last_recv_mdb_time ;

timer_flag_t sys_time_flag;

unsigned char pos_led_strip;
ADC_CHANNEL_t current_sense_key;

unsigned int time_refresh_led;//in 100ms
MSG_DISPLAY_t msg_disp16;
char messaggio[20];
Keys_Visel_t keys_visel[NUM_MAX_SHIFTREG];
KeysTest_Visel_t test_visel;
#endif /* INC_GLOBALS_H_ */
