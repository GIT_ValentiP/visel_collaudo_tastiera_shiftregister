/*
 ******************************************************************************
 *  @file      : led_driver_PCA9685.h
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 26 lug 2019
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : SANREMO_Opera 
 *   
 *  @brief     : Cortex-M0 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *  This is a library for PA9685 PWM led & Servo driver.
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */

#ifndef INC_LED_DRIVER_PCA9685_H_
#define INC_LED_DRIVER_PCA9685_H_


/* Includes ----------------------------------------------------------*/
#include "main.h"
/* Typedef -----------------------------------------------------------*/

/* Define ------------------------------------------------------------*/
#define PCA9685_ADDRESS 0x40
#define PCA9685_SUBADR1 0x2 /**< i2c bus address 1 */
#define PCA9685_SUBADR2 0x3 /**< i2c bus address 2 */
#define PCA9685_SUBADR3 0x4 /**< i2c bus address 3 */

#define PCA9685_MODE1 0x0 /**< Mode Register 1 */
#define PCA9685_MODE2 0x1 /**< Mode Register 2 */
#define PCA9685_PRESCALE 0xFE /**< Prescaler for PWM output frequency */

#define LED0_ON_L 0x6 /**< LED0 output and brightness control byte 0 */
#define LED0_ON_H 0x7 /**< LED0 output and brightness control byte 1 */
#define LED0_OFF_L 0x8 /**< LED0 output and brightness control byte 2 */
#define LED0_OFF_H 0x9 /**< LED0 output and brightness control byte 3 */

#define ALLLED_ON_L 0xFA /**< load all the LEDn_ON registers, byte 0 */
#define ALLLED_ON_H 0xFB /**< load all the LEDn_ON registers, byte 1 */
#define ALLLED_OFF_L 0xFC /**< load all the LEDn_OFF registers, byte 0 */
#define ALLLED_OFF_H 0xFD /**< load all the LEDn_OFF registers, byte 1 */

#define LED2_RED     0 /**< LED on Channel 0*/ /* Schematic rev1 DL6*/
#define LED2_GREEN   1 /**< LED on Channel 1*/
#define LED2_BLUE    2 /**< LED on Channel 2*/

#define LED1_RED     3 /**< LED on Channel 3*/ /* Schematic rev1 DL7*/
#define LED1_GREEN   4 /**< LED on Channel 4*/
#define LED1_BLUE    5 /**< LED on Channel 5*/

#define LED3_RED     6 /**< LED on Channel 6*//* Schematic rev1 DL8*/
#define LED3_GREEN   7 /**< LED on Channel 7*/
#define LED3_BLUE    8 /**< LED on Channel 8*/

#define LED4_RED     9 /**< LED on Channel 9*/  /* Schematic rev1 DL9*/
#define LED4_GREEN  10 /**< LED on Channel 10*/
#define LED4_BLUE   11 /**< LED on Channel 11*/

#define LED5_RED    12 /**< LED on Channel 12*/ /* Schematic rev1 DL10*/
#define LED5_GREEN  13 /**< LED on Channel 13*/
#define LED5_BLUE   14 /**< LED on Channel 14*/

#define LED6_RED_GREEN_BLUE     15 /**< LED on Channel 15*/ /* Schematic rev1 DL11*/


/* Macro -------------------------------------------------------------*/


/* Variables ---------------------------------------------------------*/
  uint8_t PCA9585_i2caddr;

  uint8_t PCA9685_prescale ;
  bool PCA9685_invert;
  //TwoWire * PCA9585_i2c;




/* Function prototypes -----------------------------------------------*/
  // Costruttore PCA9585_PWMLedDriver(uint8_t addr = 0x40, TwoWire *I2C = &Wire);
  void PCA9685_begin(uint8_t prescale);
  void PCA9685_reset();
  void PCA9685_sleep();
  void PCA9685_wakeup();
  void PCA9685_setExtClk(uint8_t prescale);
  void PCA9685_setPWMFreq(float freq);
  void PCA9685_setOutputMode(bool totempole);
  uint8_t PCA9685_getPWM(uint8_t num);
  void PCA9685_setPWM(uint8_t num, uint16_t on, uint16_t off);
  void PCA9685_setPin(uint8_t num, uint16_t val, bool invert);

  uint8_t PCA9685_read8(uint8_t addr);
  void PCA9685_write8(uint8_t addr, uint8_t d);
  void PCA9685_setColorLed(uint8_t numLed, uint8_t color);
  void PCA9685_setDutyLed(uint8_t numLed, uint8_t red,uint8_t green,uint8_t blue);
  void PCA9685_RGB_Transition(uint8_t nled,uint8_t status, uint16_t passo_duty);
  void PCA9685_Init_Default(void);

/***********************
  I2C Defines & Variable
************************/

/*
#define PCA9685_MODE1 0x00
#define PCA9685_MODE2 0x01

#define PCA9685_WHITE_ADDRESS 0x41
#define PCA9685_BLUE_ADDRESS 0x40

#define PCA9685_ALLCALL 0x01
#define PCA9685_SLEEP 0x10
#define PCA9685_AI 0x20
#define PCA9685_OUTDRV 0x04
#define PCA9685_PRE_SCALE 0xFE
#define PCA9685_MAX_DUTY_CICLE 4095
#define PCA9685_REGISTERS_PER_CHANNEL 4
#define PCA9685_LED0_ON_L 0x06



void PCA9685_SET_MODE1(unsigned char Serial_Config_Command, unsigned char PCA9685_Address);
void PCA9685_SET_MODE2(unsigned char Serial_Config_Command, unsigned char PCA9685_Address);
void PCA9685_SET_PRESCALE(unsigned char Serial_Config_Command, unsigned char PCA9685_Address);
void PCA9685_ALL_LED_WHITE_OFF(unsigned char Serial_Config_Variable_1, unsigned char Serial_Config_Variable_2);
void PCA9685_ALL_LED_BLUE_OFF(unsigned char Serial_Config_Variable_1, unsigned char Serial_Config_Variable_2);
void PCA9685_ALL_LED_WHITE_ON(unsigned char Serial_Config_Variable_1, unsigned char Serial_Config_Variable_2);
void PCA9685_ALL_LED_BLUE_ON(unsigned char Serial_Config_Variable_1, unsigned char Serial_Config_Variable_2);
void PCA9685_SET_LED_WHITE_ON(unsigned char Serial_Config_Variable_2, unsigned char Serial_Config_Variable_1, unsigned char Index_LED);
void PCA9685_SET_LED_BLUE_ON(unsigned char Serial_Config_Variable_2, unsigned char Serial_Config_Variable_1, unsigned char Index_LED);


void PCA9685_Init_Default(void);
void PCA9685_Init_LED_Default(void);
//void PCA9685_ALL_LED_BLUE_ON(unsigned char PWM);
//void PCA9685_ALL_LED_WHITE_ON(unsigned char PWM);
void PCA9685_Init_LED_SW1(void);
void PCA9685_Init_LED_SW2(void);

*/



/*!
 *  @brief  Class that stores state and functions for interacting with PCA9685 PWM chip
 */



#endif /* INC_LED_DRIVER_PCA9685_H_ */
