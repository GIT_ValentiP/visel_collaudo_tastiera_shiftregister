/*
 * mdb_registers.h
 *
 *  Created on: 04 ago 2019
 *      Author: VALENTI
 */

#ifndef INC_MODBUS_DMA_MDB_REGISTERS_H_
#define INC_MODBUS_DMA_MDB_REGISTERS_H_


#include "main.h"

#define MDB_OK						(0)
#define MDB_ERR_ILLEGAL_FUNCTION    (1)
#define MDB_ERR_ILLEGAL_DATAADDRESS (2)
#define MDB_ERR_ILLEGAL_DATAVALUE   (3)


/* Funzioni di lettura:
 * in: index, indice modbus (0-65535)
 * out: >=0 valore letto (0-65535)
 *      <0 errore modbus negato (es: -ILLEGAL_DATAADDRESS)
 */
int32_t mdb_read_status(uint16_t index);
int32_t mdb_read_coil(uint16_t index);
int32_t mdb_read_input_register(uint16_t index);
int32_t mdb_read_holding_register(uint16_t index);

/* Funzioni di scrittura:
 * in: index: indice modbus (0-65535)
 *     value: valore da scrivere (0-65535)
 * out: errore modbus negato (es: 0=ok, -ILLEGAL_DATAADDRESS)
 */
int32_t mdb_write_coil(uint16_t index, uint16_t value);
int32_t mdb_write_holding_register(uint16_t index, uint16_t value);



#endif /* INC_MODBUS_DMA_MDB_REGISTERS_H_ */
