/*
 * mdb.h
 *
 *  Created on: 04 ago 2019
 *      Author: VALENTI
 */

#ifndef INC_MODBUS_DMA_MDB_H_
#define INC_MODBUS_DMA_MDB_H_

#include "main.h"

#define MDB_BROADCAST_ADDR	((uint8_t)0)

uint32_t mdb_receive_message(uint8_t* rcv_data, uint32_t recv_num, uint8_t* reply_buffer, uint32_t reply_max_size);

void mdb_serial_setSlaveAddress(uint8_t slave_address);
uint32_t mdb_serial_receiveMessage(uint8_t* rcv_data, uint32_t rcv_len, uint8_t* txBuffer, uint32_t txBufferLen);


#endif /* INC_MODBUS_DMA_MDB_H_ */
