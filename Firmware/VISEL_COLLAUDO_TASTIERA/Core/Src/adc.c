/**
  ******************************************************************************
  * File Name          : ADC.c
  * Description        : This file provides code for the configuration
  *                      of the ADC instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "adc.h"

/* USER CODE BEGIN 0 */
#include "globals.h"
/* USER CODE END 0 */

ADC_HandleTypeDef hadc;

/* ADC init function */
void MX_ADC_Init(void)
{
  ADC_ChannelConfTypeDef sConfig = {0};

  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
  */
  hadc.Instance = ADC1;
  hadc.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;//ADC_CLOCK_ASYNC_DIV1;
  hadc.Init.Resolution = ADC_RESOLUTION_12B;
  hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc.Init.ScanConvMode = ADC_SCAN_DIRECTION_FORWARD;
  hadc.Init.EOCSelection = ADC_EOC_SINGLE_CONV;//ADC_EOC_SEQ_CONV;//
  hadc.Init.LowPowerAutoWait = DISABLE;
  hadc.Init.LowPowerAutoPowerOff = DISABLE;
  hadc.Init.ContinuousConvMode = DISABLE;
  hadc.Init.DiscontinuousConvMode = DISABLE;
  hadc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc.Init.DMAContinuousRequests = DISABLE;
  hadc.Init.Overrun =ADC_OVR_DATA_OVERWRITTEN;// ADC_OVR_DATA_PRESERVED;
  if (HAL_ADC_Init(&hadc) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel to be converted. 
  */
  sConfig.Channel = ADC_CHANNEL_10;
  sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;
  sConfig.SamplingTime = ADC_SAMPLETIME_71CYCLES_5;//ADC_SAMPLETIME_1CYCLE_5;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

}

void HAL_ADC_MspInit(ADC_HandleTypeDef* adcHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(adcHandle->Instance==ADC1)
  {
  /* USER CODE BEGIN ADC1_MspInit 0 */

  /* USER CODE END ADC1_MspInit 0 */
    /* ADC1 clock enable */
    __HAL_RCC_ADC1_CLK_ENABLE();
  
    __HAL_RCC_GPIOC_CLK_ENABLE();
    /**ADC GPIO Configuration    
    PC0     ------> ADC_IN10 
    */
    GPIO_InitStruct.Pin = CURRENT_KEYS_ADC10_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(CURRENT_KEYS_ADC10_GPIO_Port, &GPIO_InitStruct);

    /* ADC1 interrupt Init */
    HAL_NVIC_SetPriority(ADC1_COMP_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(ADC1_COMP_IRQn);
  /* USER CODE BEGIN ADC1_MspInit 1 */
    /* Run the ADC calibration */
    if (HAL_ADCEx_Calibration_Start(adcHandle) != HAL_OK)
	  {
		/* Calibration Error */
		Error_Handler();
	  }
  /* USER CODE END ADC1_MspInit 1 */
  }
}

void HAL_ADC_MspDeInit(ADC_HandleTypeDef* adcHandle)
{

  if(adcHandle->Instance==ADC1)
  {
  /* USER CODE BEGIN ADC1_MspDeInit 0 */

  /* USER CODE END ADC1_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_ADC1_CLK_DISABLE();
  
    /**ADC GPIO Configuration    
    PC0     ------> ADC_IN10 
    */
    HAL_GPIO_DeInit(CURRENT_KEYS_ADC10_GPIO_Port, CURRENT_KEYS_ADC10_Pin);

    /* ADC1 interrupt Deinit */
    HAL_NVIC_DisableIRQ(ADC1_COMP_IRQn);
  /* USER CODE BEGIN ADC1_MspDeInit 1 */

  /* USER CODE END ADC1_MspDeInit 1 */
  }
} 

/* USER CODE BEGIN 1 */
void ADC_threshold(ADC_HandleTypeDef* adcHandle){
	uint8_t nsample;
	/*## Start ADC conversions ##*/
	current_sense_key.avg=0;
	current_sense_key.nsample=10;
	current_sense_key.AdcConvStatus=2;
	nsample=0;
    /* Start ADC conversion on regular group */
	while (nsample<current_sense_key.nsample){
		HAL_ADC_Start(adcHandle);
		current_sense_key.raw=0;
		if (HAL_ADC_PollForConversion(adcHandle,100) != HAL_OK)
		{
			/* Start Error */
			Error_Handler();
		}else{
	          nsample++;
			  current_sense_key.raw=HAL_ADC_GetValue(adcHandle);
			  current_sense_key.avg+=current_sense_key.raw;
			 }
		HAL_ADC_Stop(adcHandle);
	}
    if(nsample>9){//current_sense_key.nsample){
    	current_sense_key.avg=(current_sense_key.avg/current_sense_key.nsample);
    	current_sense_key.threshold=COMPUTATION_DIGITAL_12BITS_TO_VOLTAGE(current_sense_key.avg);
    }


}

bool ADC_compare(ADC_HandleTypeDef* adcHandle){

	uint32_t delta=0;
	bool compare=false;
	current_sense_key.raw=0;

	HAL_ADC_Start(adcHandle);
	if (HAL_ADC_PollForConversion(adcHandle,10) != HAL_OK)
	{
		/* Start Error */
		Error_Handler();
	}else{
		  current_sense_key.raw=HAL_ADC_GetValue(adcHandle);

		 }
	HAL_ADC_Stop(adcHandle);

	current_sense_key.mVolt=COMPUTATION_DIGITAL_12BITS_TO_VOLTAGE(current_sense_key.raw);
	if(current_sense_key.mVolt>current_sense_key.threshold){
		delta=(current_sense_key.mVolt-current_sense_key.threshold);
	}else{
		delta=(current_sense_key.threshold - current_sense_key.mVolt);
	}

	if(delta > CURRENT_KEY_MAX_mV){
		compare=true;
	}
	return(compare);
}
/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
