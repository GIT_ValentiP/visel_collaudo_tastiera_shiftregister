/*
SparkFun Bargraph Breakout Arduino library
Mike Grusin, SparkFun Electronics

Revision history:
1.2 05/12/2015 Updated for Arduino 1.6 - fixing directory formatting, updating file names
1.1 12/27/2011 updated for Arduino 1.0, changed begin() parameter order, fixed example which always used pin 10 for LAT, even on a Mega
1.0 10/10/2011 release

Hardware connections:

Connect the following pins on the bargraph board to your Arduino:

Name	Function		Uno pin		Mega pin

+5V		Power supply	5V			5V
GND		Ground			GND			GND
SIN		SPI input		11 (MOSI)	51 (MOSI)
CLK		Clock input 	13 (SCK)	52 (SCK)
LAT		Latch input 	10 (SS)		53 (SS)

Note: you can use any available pin for the LATch pin, see begin() below.

Library usage:

1. Install SparkFunBarGraph folder in your sketches/libraries folder. Restart the Arduino IDE.

2. See the example sketches, located in the Arduino IDE under examples/SFEbarGraph

3. Include the library and SPI at the begining of your sketch:

   #include <SparkFunBarGraph.h>
   #include <SPI.h>

4. Declare a variable of type SFEbarGraph (we call it "BG" here):

   SFEbarGraph BG;

5. Call "BG.begin()" to set up variables. Call without parameters for defaults (one board, 
   LAT pin = 10 for Uno etc. and 53 for Mega), or call with BG.begin(numberofboards) or
   BG.begin(numberofboards,latchpin)  

   BG.begin(); // use default pins and one board
   
6. Now you can drive the bargraph with BG.barGraph(), BG.clear(), BG.paint(), and BG.send().

License:

This code is free to use, change, improve, even sell!  All we ask for is two things:
1. That you give SparkFun credit for the original code,
2. If you sell or give it away, you do so under the same license so others can do the same thing.
More at http://creativecommons.org/licenses/by-sa/3.0/

Have fun! 
-your friends at SparkFun
*/

/* Includes ----------------------------------------------------------*/
#include "shiftreg_lib.h"
#include "spi.h"
#include "globals.h"
#include "gpio.h"
#include "iwdg.h"
/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/
extern SPI_HandleTypeDef hspi2;
/* Private global variables-----------------------------------------------------*/

unsigned char *_SFEbarGraphCanvas; // Pointer to array declared in begin()
unsigned char _SFEbarGraphLatchPin; // Latch pin
unsigned char _SFEbarGraphNumBoards; // Number of boards

void SFEbarGraph_init(void){
	HAL_GPIO_WritePin(RST_STORE_LED_GPIO_Port,RST_STORE_LED_Pin , GPIO_PIN_SET);
}


bool SFEbarGraph_begin()
// Initialize the library with default values (latch pin = 10 or 53, number of boards = 1)
{

return(true);
}



bool SFEbarGraph_begin1(unsigned char numboards)
// Initialize the library, with user-defined latch pin and default number of boards (1)
{

  if(display_refresh_start_event==0){
	  SFEbarGraph_paint(pos_led_strip, false);
	  if(pos_led_strip<((_SFEbarGraphNumBoards * NUM_OUT_SHIFTREG)-1)){
		  pos_led_strip++;
	  }else{
		  pos_led_strip=0;
	  }
	  SFEbarGraph_paint(pos_led_strip, true)	;
	  display_refresh_start_event=50;

  }

//  for(pos=0;pos<(_SFEbarGraphNumBoards * NUM_OUT_SHIFTREG);pos++){
//	   SFEbarGraph_paint(pos, true)	;
//	   display_refresh_start_event=0;
//	   while(display_refresh_start_event==0){}
//	   HAL_IWDG_Refresh(&hiwdg);
//	   SFEbarGraph_paint(pos, false)	;
//	   display_refresh_start_event=0;
//	   while(display_refresh_start_event==0){}
//	   HAL_IWDG_Refresh(&hiwdg);
//  }

 return(true);
}


bool SFEbarGraph_begin2(unsigned char numboards, unsigned char latchpin)
// Initialize the library, with user-defined latch pin and user-defined number of boards (1 to 8)
{
  if ((numboards > 8) || (numboards < 1)) // Error check
  {
//  Serial.print("SFEbarGraph::begin ERROR: illegal number of boards");
    return(false);
  }

  _SFEbarGraphNumBoards = numboards;
//  Serial.print("numboards: "); Serial.println(_SFEbarGraphNumBoards,DEC);

  _SFEbarGraphLatchPin = latchpin;
//  Serial.print("latchpin: "); Serial.println(_SFEbarGraphLatchPin,DEC);

  //pinMode(_SFEbarGraphLatchPin,OUTPUT);
	
  // The bargraph will operate at any SPI speed the Arduino is capable of.
  // If you are also using other SPI devices such as Serial 7-Segment displays,
  // you may need to slow the bus down by uncommenting the following line:
  // SPI.setClockDivider(SPI_CLOCK_DIV128);

  HAL_SPI_MspInit(&hspi2);//SPI.begin();//SPI  Init

  // Allocate 4 bytes of memory for each connected bargraph
  _SFEbarGraphCanvas = (unsigned char*)malloc(_SFEbarGraphNumBoards * NUM_MAX_SHIFTREG);

  // Make sure memory allocation was successful, return false if there was a problem
  if (_SFEbarGraphCanvas != 0)
  {
	SFEbarGraph_clear();
    return(true);
  }
  else
    return(false);
}


void SFEbarGraph_bar(unsigned char bar, unsigned char peak)
// Use the board as a traditional bargraph
// Bar = number of LEDs to light (0 for none), 
// Peak = optional "peak" overlay (0 for none) to turn on  an output of 'peak-max value'
// LED numbering starts at 1, use 0 for no LEDs lit
{
  unsigned char x, affectedByte, affectedBit;
  
  //bar += ((bar-1) / 30) * 2; // Add two bits for every 30 (shift register is 32 bits, but there are only 30 LEDs)
  
  if (bar <= (_SFEbarGraphNumBoards * NUM_OUT_SHIFTREG)) // error check
  {
    affectedByte = bar / 8; // Compute byte and bit that "bar" resides in
	affectedBit = bar % 8;

	//Serial.print(bar,DEC);
	//Serial.print(" ");
	//Serial.print(affectedByte,DEC);
	//Serial.print(" ");
	//Serial.print(affectedBit,DEC);
	//Serial.print(" ");
	//Serial.println((1 << affectedBit)-1,DEC);

	SFEbarGraph_clear(); // Zero all bytes
	
	// Fill all bytes below affected byte with 0xFF (all LEDs on)
	for (x = 0; x < affectedByte; x++)
	  _SFEbarGraphCanvas[x] = 0xFF;

	if(affectedByte<(_SFEbarGraphNumBoards * NUM_OUT_SHIFTREG)){
	// Turn on all LEDs in affectedbyte up to affectedbit (note the "-1" below)
	_SFEbarGraphCanvas[affectedByte] = (1 << affectedBit)-1;
	}
	// Handle peak LED if required
	if (peak > 0){
		SFEbarGraph_paint(peak-1,true);
	}
	
	// Send the data to the bargraph
	SFEbarGraph_send();
  }
}


// The following functions allow you to turn arbitrary LEDs on / off
// Note that all "painting" occurs in the background, and does not appear on the LEDs until send()


void SFEbarGraph_clear()
// Clear the "canvas" to all LEDs off
{
  int x;

  // Zero all bytes (four per board)
  for (x = 0; x < (_SFEbarGraphNumBoards * NUM_MAX_SHIFTREG); x++)
    _SFEbarGraphCanvas[x] = 0;
}


void SFEbarGraph_set_all(unsigned char latchpin){

	 int x;

	  // Zero all bytes (four per board)
	  for (x = 0; x < (_SFEbarGraphNumBoards * NUM_MAX_SHIFTREG); x++){
	    _SFEbarGraphCanvas[x] = latchpin;
	  }
	 // Send the data to the bargraph
	 SFEbarGraph_send();
}

void SFEbarGraph_paint(unsigned char position, bool value)
// Turn arbitrary LEDs on (1 or true or HIGH), or off (0 or false or LOW) 
// Note that LED numbering starts at 0
{
  unsigned char affectedByte, affectedBit;

  //Serial.print(position,DEC);
  
  //position += (position / NUM_OUT_SHIFTREG) * 2; // Add two bits for every 30 (shift register is 32 bits, but there are only 30 LEDs)

  if (position < (_SFEbarGraphNumBoards * NUM_OUT_SHIFTREG)) // Error check
  {
    affectedByte = position / 8; // Compute byte and bit that "position" resides in
    affectedBit = position % 8;

    //Serial.print(" ");
    //Serial.print(position,DEC);
    //Serial.print(" ");
    //Serial.print(affectedByte,DEC);
    //Serial.print(" ");
    //Serial.println(affectedBit,DEC);

    if (value) // Make bit 1
      _SFEbarGraphCanvas[affectedByte] |= (1 << affectedBit); // Mask and set bit
    else // Make bit 0
      _SFEbarGraphCanvas[affectedByte] &= ~(1 << affectedBit); // Mask and clear bit

    // Send the data to the bargraph
    	SFEbarGraph_send();
  }
}


void SFEbarGraph_send()
// Send the current "canvas" to the bargraph board
// LEDs will remain fixed until next send()
{
  unsigned char x;
  uint8_t pData[5];

  HAL_GPIO_WritePin(RST_STORE_LED_GPIO_Port,RST_STORE_LED_Pin , GPIO_PIN_SET);//
  HAL_GPIO_WritePin(SPI2_NSS_STROBECLK_GPIO_Port,SPI2_NSS_STROBECLK_Pin , GPIO_PIN_RESET);//digitalWrite(_SFEbarGraphLatchPin,LOW); // Make LAT low

  // Send all bytes to board(s) (four bytes per board)
  for (x = (_SFEbarGraphNumBoards*NUM_MAX_SHIFTREG); x > 0; x--){
	  pData[0]=(_SFEbarGraphCanvas[x-1]);
	  HAL_SPI_Transmit(&hspi2,pData,1, 1000);//SPI.transfer(_SFEbarGraphCanvas[x-1]);
  }
  HAL_GPIO_WritePin(SPI2_NSS_STROBECLK_GPIO_Port,SPI2_NSS_STROBECLK_Pin , GPIO_PIN_SET);//digitalWrite(_SFEbarGraphLatchPin,HIGH); // Make LAT high, L-H transition causes data to appear at LEDs
}


void SFEKeyboard_receive(void)
// Read the binary representation of
// Least-significant bit goes to lowest
// Note
{
  uint8_t x;
  //unsigned char *z;
  //unsigned char a;
  uint8_t pData[NUM_MAX_SHIFTREG];
  HAL_GPIO_WritePin(RST_STORE_LED_GPIO_Port,RST_STORE_LED_Pin , GPIO_PIN_RESET);//
  HAL_GPIO_WritePin(SPI2_NSS_STROBECLK_GPIO_Port,SPI2_NSS_STROBECLK_Pin , GPIO_PIN_RESET);//digitalWrite(_SFEbarGraphLatchPin,LOW); // Make LAT low
  for(x=0;x<10;x++){

  }
  // A long is four bytes
  // z = (unsigned char*)&number; // pointer to first byte
  //HAL_SPI_Receive(SPI_HandleTypeDef *hspi, uint8_t *pData, uint16_t Size, uint32_t Timeout);
  HAL_GPIO_WritePin(SPI2_NSS_STROBECLK_GPIO_Port,SPI2_NSS_STROBECLK_Pin , GPIO_PIN_SET);//digitalWrite(_SFEbarGraphLatchPin,HIGH); // Make LAT high, L-H transition causes data to appear at LEDs

  HAL_SPI_Receive(&hspi2,pData,NUM_MAX_SHIFTREG, 1000);
  // Retrieve each byte from memory
  for (x = 0; x < NUM_MAX_SHIFTREG; x++)
  {
	 // get each byte in the long
     //HAL_SPI_Transmit(&hspi2,pData,1, 1000);//SPI.transfer(a); // send it to the bargraph
	  keys_visel[(NUM_MAX_SHIFTREG-1)-x].raw=(uint8_t)pData[x];
  }
  HAL_GPIO_WritePin(RST_STORE_LED_GPIO_Port,RST_STORE_LED_Pin , GPIO_PIN_SET);//
}



void SFEbarGraph_sendLong(unsigned char *number)
// Sends the binary representation of "number" directly to the bargraph board
// Least-significant bit goes to lowest LED
// Note that this only works effectively for one bargraph board
{
  char x;
  //unsigned char *z;
  //unsigned char a;
  uint8_t pData[5];
  HAL_GPIO_WritePin(RST_STORE_LED_GPIO_Port,RST_STORE_LED_Pin , GPIO_PIN_SET);//
  HAL_GPIO_WritePin(SPI2_NSS_STROBECLK_GPIO_Port,SPI2_NSS_STROBECLK_Pin , GPIO_PIN_RESET);//digitalWrite(_SFEbarGraphLatchPin,LOW); // Make LAT low

  // A long is four bytes
 // z = (unsigned char*)&number; // pointer to first byte

  // Retrieve each byte from memory
  for (x = NUM_MAX_SHIFTREG; x >= 0; x--)
  {

	pData[0]=(uint8_t) *(number+x); // get each byte in the long

    //HAL_SPI_Transmit(SPI_HandleTypeDef *hspi, uint8_t *pData, uint16_t Size, uint32_t Timeout);
    HAL_SPI_Transmit(&hspi2,pData,1, 1000);//SPI.transfer(a); // send it to the bargraph

  }
  HAL_GPIO_WritePin(SPI2_NSS_STROBECLK_GPIO_Port,SPI2_NSS_STROBECLK_Pin , GPIO_PIN_SET);//digitalWrite(_SFEbarGraphLatchPin,HIGH); // Make LAT high, L-H transition causes data to appear at LEDs
}
