/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dma.h"
#include "i2c.h"
#include "iwdg.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "usb_device.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "display_16segments.h"
#include "version_types.h"
#include "uart_dma_process.h"
#include "globals.h"
#include "mdb_com.h"
#include "shiftreg_lib.h"
/*Modbus Library */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
#ifdef __GNUC__
  /* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
     set to 'Yes') calls __io_putchar() */
  #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
  #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */


/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */


/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
//__attribute__((section(".projectvars"))) const uint32_t  VERSION_NUMBER = ((FW_VERSION<<16)| FW_SUBVERSION);
//__attribute__((section(".projectvars"))) const uint32_t  CRC_PROGRAM= 0x000000000;
//__attribute__((section(".projectvars"))) const uint16_t  BUILD_ID=0xA5A5;
//__attribute__((section(".projectvars"))) const  uint8_t  OTHER_VAR=0x00;
const uint32_t  VERSION_NUMBER = ((FW_VERSION<<16)| FW_SUBVERSION);
const uint32_t  CRC_PROGRAM= 0x000000000;
const uint16_t  BUILD_ID=0xA5A5;
const  uint8_t  OTHER_VAR=0x00;
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
unsigned char new_key_pressed,enc_rotation_read;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_NVIC_Init(void);
/* USER CODE BEGIN PFP */
void delay_ms(unsigned long millisec);
void app_main_lab(void);
void app_main(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */


  //ENCODER_Rotate_t enc_rotation_read;
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */


  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_IWDG_Init();
  MX_I2C1_Init();
  MX_SPI2_Init();
  MX_TIM6_Init();
  MX_TIM7_Init();
  MX_TIM14_Init();
 // MX_TIM16_Init();
 // MX_TIM17_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  //MX_USB_DEVICE_Init();
  MX_ADC_Init();
  HAL_ADC_MspInit(&hadc);
  /* Initialize interrupts */
  MX_NVIC_Init();
  /* USER CODE BEGIN 2 */

  version_fw=VERSION_NUMBER;



  Main_Clock_Init();

  HAL_TIM_Base_MspInit(&htim14);
  HAL_I2C_MspInit(&hi2c1);


  HAL_IWDG_Refresh(&hiwdg);
  ADC_threshold(&hadc);

  DP_SW_RS485_adr=OPERA_RS485_ADDR;//address  to identify OPERA Keyboard on ModBus
  HAL_IWDG_Refresh(&hiwdg);

  RS485_TX_DISABLE;
  RS485_RX_ENABLE;
  printf("\n\r *** VISEL COLLAUDO TASTIERA: %3d.%3d ***\n\r",FW_VERSION,FW_SUBVERSION);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  /*TURN ON ALL SEGMENTS*/
  HAL_IWDG_Refresh(&hiwdg);
//  Display16Seg_Start();//Animazione su display 16segmenti

//  char_disp_17seg=32;
//  Display16Seg_Char( char_disp_17seg ,0,ON_DISP);
//  new_key_pressed=17;
//  enc_rotation_read=17;
//  char_rx_to_display=0xFF;
  HAL_IWDG_Refresh(&hiwdg);
  PCA9685_Init_Default(); //LED driver PCA9685 init

  new_data_rx=false;//flag for new data from RS485/RS232

  opera_keyboard_conf=OPERA_NOT_SET;
  opera_state=OPERA_NOT_CONFIG;

  RS485_TX_DISABLE;
  RS485_RX_ENABLE;

  init_input_reayboard();

  HAL_IWDG_Refresh(&hiwdg);
  //mdb_comm_Initialize(); //Modbus Communication Init
  timeout_mdb_no_msg=100;//TIMEOUT_100ms_NO_MSG_MDB*2;
  //HAL_UART_Receive_DMA(&huart1, (uint8_t *)buffer_RX_RS232, LEN_RX_RS232);
  //HAL_UART_Receive_DMA(&huart2, (uint8_t *)buffer_RX_RS232, LEN_RX_RS232);//RS485

  sprintf(messaggio,"*Start*");  //scanning the whole string, including the white spaces
  //Display16Seg_Msg((char *)messaggio ,(strlen(messaggio)-1));
  Display16Seg_Num( 0 ,0,ON_DISP);
  SFEbarGraph_begin2(1,10);
  SFEbarGraph_begin();//SFEbarGraph_paint(12,true);
  change_state=true;

  test_visel.tot_press=0;
  clear_test_Visel();
  test_visel.status=VISEL_RESTART_TEST;//(VISEL_IDLE);
  while (1)
  {
	HAL_IWDG_Refresh(&hiwdg);
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	if (read_keyboard_event==1){
	 read_input_keyboard();
	 read_keyboard_event=0;
	}

	app_main();
	if (change_state==true){//received new duty for led
		 PCA9685_setDutyLed(SW1_LED_RGB*3, led_RGB[SW1_LED_RGB].duty_RED,led_RGB[SW1_LED_RGB].duty_GREEN,led_RGB[SW1_LED_RGB].duty_BLUE);
		 PCA9685_setDutyLed(SW2_LED_RGB*3, led_RGB[SW2_LED_RGB].duty_RED,led_RGB[SW2_LED_RGB].duty_GREEN,led_RGB[SW2_LED_RGB].duty_BLUE);
		 PCA9685_setDutyLed(SW3_LED_RGB*3, led_RGB[SW3_LED_RGB].duty_RED,led_RGB[SW3_LED_RGB].duty_GREEN,led_RGB[SW3_LED_RGB].duty_BLUE);
		 PCA9685_setDutyLed(SW4_LED_RGB*3, led_RGB[SW4_LED_RGB].duty_RED,led_RGB[SW4_LED_RGB].duty_GREEN,led_RGB[SW4_LED_RGB].duty_BLUE);
		 //Display16Seg_Char(char_rx_to_display,0,ON_DISP);
		 change_state=false;
    }
	PCA9685_setDutyLed(SW5_LED_RGB*3, led_RGB[SW5_LED_RGB].duty_RED,led_RGB[SW5_LED_RGB].duty_GREEN,led_RGB[SW5_LED_RGB].duty_BLUE);
    PCA9685_setDutyLed(SW6_LED_RGB*3, led_RGB[SW6_LED_RGB].duty_RED,led_RGB[SW6_LED_RGB].duty_GREEN,led_RGB[SW6_LED_RGB].duty_BLUE);
//	if (opera_keyboard_conf==OPERA_4_TASTI){
//	  Opera_keyboard_function_4Tasti();
//	}else if(opera_keyboard_conf==OPERA_7_TASTI){
//	  Opera_keyboard_function_7Tasti();
//	}
//  mdb_comm_processMessage();/*Modbus poll update in each run*/

	Display16Seg_design();
    //HAL_GPIO_WritePin(RST_STORE_LED_GPIO_Port,RST_STORE_LED_Pin , TOGGLE_PIN_LED);

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI14|RCC_OSCILLATORTYPE_LSI
                              |RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSI14State = RCC_HSI14_ON;
  RCC_OscInitStruct.HSI14CalibrationValue = 16;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL3;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB|RCC_PERIPHCLK_USART1
                              |RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_I2C1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_SYSCLK;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLL;

  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief NVIC Configuration.
  * @retval None
  */
static void MX_NVIC_Init(void)
{
  /* EXTI4_15_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(EXTI4_15_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);
  /* DMA1_Channel2_3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel2_3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel2_3_IRQn);
  /* DMA1_Channel4_5_6_7_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel4_5_6_7_IRQn, 0, 0);
//  HAL_NVIC_EnableIRQ(DMA1_Channel4_5_6_7_IRQn);
  /* TIM6_DAC_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM6_DAC_IRQn, 0, 0);
//  HAL_NVIC_EnableIRQ(TIM6_DAC_IRQn);
  /* TIM7_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM7_IRQn, 0, 0);
//  HAL_NVIC_EnableIRQ(TIM7_IRQn);
  /* TIM14_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM14_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(TIM14_IRQn);
  /* TIM16_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM16_IRQn, 0, 0);
//  HAL_NVIC_EnableIRQ(TIM16_IRQn);
  /* TIM17_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM17_IRQn, 0, 0);
//  HAL_NVIC_EnableIRQ(TIM17_IRQn);
  /* I2C1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(I2C1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(I2C1_IRQn);
  /* USART1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(USART1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(USART1_IRQn);
  /* USART2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(USART2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(USART2_IRQn);
  /* USB_IRQn interrupt configuration */
//  HAL_NVIC_SetPriority(USB_IRQn, 0, 0);
//  HAL_NVIC_EnableIRQ(USB_IRQn);
}

/* USER CODE BEGIN 4 */
void delay_ms(unsigned long millisec)
{
	unsigned long cnt_to_ms,iter1,iter2;
	cnt_to_ms=6000;

	for(iter2=0;iter2 < millisec;iter2++){
		for(iter1=0;iter1 < cnt_to_ms;iter1++){
				__NOP();
			}
	}

}


/**
  * @brief  Retargets the C library printf function to the USART.
  * @param  None
  * @retval None
  */
PUTCHAR_PROTOTYPE
{
  /* Place your implementation of fputc here */
  /* e.g. write a character to the EVAL_COM1 and Loop until the end of transmission */

 HAL_UART_Transmit(&huart1, (uint8_t *)&ch, 1, 0xFFFF);
 // HAL_UART_Transmit(&huart2, (uint8_t *)&ch, 1, 0xFFFF);
//	RS485_RX_DISABLE; //RS485 Half Duplex Direction in TX
//	RS485_TX_ENABLE;
//	HAL_UART_Transmit(&huart2, (uint8_t *)&ch, 1, 0xFFFF);//On RS485
//	RS485_TX_DISABLE; //RS485 Half Duplex Direction in TX
//	RS485_RX_ENABLE;

  return ch;
}

void app_main(void){
	uint8_t i;

	 switch(test_visel.status){

			 case(VISEL_IDLE):
//	                                    	if (change_state==true){
//	                                    	  for(i=0;i<4;i++){
//												led_RGB[i].duty_RED=0;
//												led_RGB[i].duty_GREEN=0;
//												led_RGB[i].duty_BLUE=100;
//	                                    	  }
//	                                    	}
//			                                if(test_visel.tot_press>0){
//			                                	test_visel.status=VISEL_START_TEST;
//			                                	sprintf(messaggio,"*Test RUN*");  //scanning the whole string, including the white spaces
//			                                    Display16Seg_Msg((char *)messaggio ,(strlen(messaggio)-1));
//			                                	for(i=0;i<4;i++){
//													led_RGB[i].duty_RED=100;
//													led_RGB[i].duty_GREEN=0;
//													led_RGB[i].duty_BLUE=0;
//												  }
//			                                	change_state=true;
//			                                }

											for(i=0;i<4;i++){
												led_RGB[i].duty_RED=100;
												led_RGB[i].duty_GREEN=0;
												led_RGB[i].duty_BLUE=0;

											}
			                                change_state=false;
			                                pos_led_strip=0;
			                                if(keyboard_1click.sw.sw1_l==1){
			                                	test_visel.status=VISEL_TEST1;
			                                	Display16Seg_Num( 1 ,0,ON_DISP);
			                                	led_RGB[0].duty_RED=0;
												led_RGB[0].duty_GREEN=100;
												led_RGB[0].duty_BLUE=0;

			                                	SFEbarGraph_set_all(0xFF);
			                                	keyboard_1click.bitmap=0x0000;
			                                	change_state=true;
											}else if(keyboard_1click.sw.sw2_l==1){
												    test_visel.status=VISEL_TEST2;
												    Display16Seg_Num( 2 ,0,ON_DISP);
													led_RGB[1].duty_RED=0;
													led_RGB[1].duty_GREEN=100;
													led_RGB[1].duty_BLUE=0;

													keyboard_1click.bitmap=0x0000;
													change_state=true;
												  }else if(keyboard_1click.sw.sw3_l==1){
													     test_visel.status=VISEL_TEST3;
													     Display16Seg_Num( 3 ,0,ON_DISP);
													     led_RGB[2].duty_RED=0;
														 led_RGB[2].duty_GREEN=100;
														 led_RGB[2].duty_BLUE=0;

														 keyboard_1click.bitmap=0x0000;
														 change_state=true;
														}else if(keyboard_1click.sw.sw4_l==1){
															     test_visel.status=VISEL_TEST4;
															     Display16Seg_Num( 4 ,0,ON_DISP);
															     led_RGB[3].duty_RED=0;
																 led_RGB[3].duty_GREEN=100;
																 led_RGB[3].duty_BLUE=0;

																 keyboard_1click.bitmap=0x0000;
																 change_state=true;
															  }
											break;

			 case(VISEL_START_TEST):
										    if(test_visel.tot_press==(NUM_OUT_SHIFTREG-4)){
												test_visel.status=VISEL_END_TEST;
												 sprintf(messaggio,"*OK*");  //scanning the whole string, including the white spaces
											     Display16Seg_Msg((char *)messaggio ,(strlen(messaggio)-1));
												 for(i=0;i<4;i++){
													led_RGB[i].duty_RED=0;
													led_RGB[i].duty_GREEN=100;
													led_RGB[i].duty_BLUE=0;
												  }
												change_state=true;
											}

											break;

			 case(VISEL_RUN):


											break;

			 case(VISEL_END_TEST):

											break;
			 case(VISEL_TEST1):
										    if(keyboard_1click.sw.sw1_l==1){
										    	SFEbarGraph_set_all(0x00);
										    	for(i=0;i<4;i++){
													led_RGB[i].duty_RED=0;
													led_RGB[i].duty_GREEN=0;
													led_RGB[i].duty_BLUE=100;
												}
										    	Display16Seg_Num( 0 ,0,ON_DISP);
										    	test_visel.status=VISEL_IDLE;
										    	change_state=true;
											}else{
												test_visel.status=VISEL_TEST1;
											}
										    keyboard_1click.bitmap=0x0000;


											break;
			 case(VISEL_TEST2):
											if(keyboard_1click.sw.sw2_l==1){
												SFEbarGraph_set_all(0x00);
												for(i=0;i<4;i++){
													led_RGB[i].duty_RED=0;
													led_RGB[i].duty_GREEN=0;
													led_RGB[i].duty_BLUE=100;
												}
												Display16Seg_Num( 0 ,0,ON_DISP);
												test_visel.status=VISEL_IDLE;
												change_state=true;
											}else{
												SFEbarGraph_begin1(0);
												test_visel.status=VISEL_TEST2;
											}
											keyboard_1click.bitmap=0x0000;

											break;
			 case(VISEL_TEST3):
		                                   if(keyboard_1click.sw.sw3_l==1){
												SFEbarGraph_set_all(0x00);
												for(i=0;i<4;i++){
													led_RGB[i].duty_RED=0;
													led_RGB[i].duty_GREEN=0;
													led_RGB[i].duty_BLUE=100;
												}
												Display16Seg_Num( 0 ,0,ON_DISP);
												test_visel.status=VISEL_IDLE;
												change_state=true;
											}else{
												scan_read_Visel();
												test_visel.status=VISEL_TEST3;
											}
											keyboard_1click.bitmap=0x0000;

											break;
			 case(VISEL_TEST4):
		                                   if(keyboard_1click.sw.sw4_l==1){
												SFEbarGraph_set_all(0x00);
												for(i=0;i<4;i++){
													led_RGB[i].duty_RED=0;
													led_RGB[i].duty_GREEN=0;
													led_RGB[i].duty_BLUE=100;
												}
												Display16Seg_Num( 0 ,0,ON_DISP);
												test_visel.status=VISEL_IDLE;
												change_state=true;
											}else{
												scan_if_pressed_Visel();
												test_visel.status=VISEL_TEST4;
											}
											keyboard_1click.bitmap=0x0000;
											break;

			 case(VISEL_RESTART_TEST):
		                                  for(i=0;i<4;i++){
												led_RGB[i].duty_RED=0;
												led_RGB[i].duty_GREEN=0;
												led_RGB[i].duty_BLUE=100;
										   }
			                               change_state=true;

										   test_visel.status=(VISEL_IDLE);
										   test_visel.tot_press=0;
										   break;

			 case(VISEL_ERROR):
											break;

			 default:
				         break;
	 }

	 if(current_sense_key.threshold>3000){
			led_RGB[4].duty_RED=100;
			led_RGB[4].duty_GREEN=0;
			led_RGB[4].duty_BLUE=100;

	 }else if(current_sense_key.threshold>2600){
			led_RGB[4].duty_RED=0;
			led_RGB[4].duty_GREEN=0;
			led_RGB[4].duty_BLUE=100;

	 }else if(current_sense_key.threshold>2560){
			led_RGB[4].duty_RED=100;
			led_RGB[4].duty_GREEN=0;
			led_RGB[4].duty_BLUE=0;

	       }else if(current_sense_key.threshold>2545){
				led_RGB[4].duty_RED=0;
				led_RGB[4].duty_GREEN=100;
				led_RGB[4].duty_BLUE=0;

		      }
	 if(sys_time_flag.flag_10ms==1){
		 if(ADC_compare(&hadc)==true){
				led_RGB[5].duty_RED=100;
		 }else{
				led_RGB[5].duty_RED=0;
		 }
		 sys_time_flag.flag_10ms=0;
	 }
//	 if(sys_time_flag.flag_1sec==1){
//		 printf("Tasti 1�shift:%2X \n\r",keys_visel[0].raw);
//		 printf("Tasti 2�shift:%2X \n\r",keys_visel[1].raw);
//		 printf("Tasti 3�shift:%2X \n\r",keys_visel[2].raw);
//		 printf("Tasti 4�shift:%2X \n\r",keys_visel[3].raw);
//		 printf("Tasti 5�shift:0x%2X \n\r",keys_visel[4].raw);
//		 printf("Tot tasti premuti:%2d\n\r", test_visel.tot_press);
//		  sys_time_flag.flag_1sec=0;
//	 }
}

void app_main_lab(void){




}

/* USER CODE END 4 */

 /**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM1 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM1) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
